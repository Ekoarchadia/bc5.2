import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch'; 

@Injectable()
export class ApiService {

eventlist: object[];
  constructor(private http:Http, private router: Router) {
     
    let token = localStorage.getItem('token');
  
    this.http
    .get('http://localhost:8000/api/userlist?token=' + token)
    .map(result => result.json())
    .subscribe(result => this.eventlist = result);
   }

   reload()
    {
      let token = localStorage.getItem("token");
        let headers = new Headers({ "Authorization" : "Bearer " + token });
        let options = new RequestOptions({ headers : headers });

        // console.log(token);

        return this.http.get('http://localhost:8000/api/userlist?token=' + token)
        .map(result => result.json())
        .subscribe(result=>this.eventlist= result);
    }

  getData(){

    let token = localStorage.getItem('token');
  
    return this.http
    .get('http://localhost:8000/api/userlist?token=' + token)
    .map(result => result.json());
  }

  addData(obj: Object){

    let token = localStorage.getItem('token');

    let body = JSON.stringify(obj); 
    let headers = new Headers({ "Content-Type" : "application/json"});
    let options = new RequestOptions({ headers : headers });

    this.http.post('http://localhost:8000/api/userlist/add?token=' + token, body, options)
     .subscribe(
      result => {
        localStorage.setItem('message', result.json().message);
        this.reload();
      },
      err => {
        localStorage.setItem('message', err.json().message);
      }
     );

  }

  deleteData(obj: Object){

    let token = localStorage.getItem('token');

    let body = JSON.stringify(obj); 
    let headers = new Headers({ "Content-Type" : "application/json" });
    let options = new RequestOptions({ headers : headers });

    this.http.post('http://localhost:8000/api/userlist/delete?token=' + token, body, options)
    .subscribe(
      result => {
        localStorage.setItem('message', result.json().message);
        this.reload();
      },
      err => {
        localStorage.setItem('message', err.json().message);
      }
     );
  }

}
