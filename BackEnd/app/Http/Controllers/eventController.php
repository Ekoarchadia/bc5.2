<?php

namespace App\Http\Controllers;

use App\event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class eventController extends Controller
{
    public function getEvent(){
        DB::beginTransaction();
        try
        {
            $event = events::get();
            DB::commit();
            return response()->json($event, 200);
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=> $e->getMessage ], 500);
        }
    }

    public function saveEvent(Request $request){
        DB::beginTransaction();

        try{
                $newEvent = new event;
                $newEvent->nama_event = $request->input('nama_event');
                $newEvent->tanggal_event = $request->input('tanggal_event');
                $newEvent->harga_tiket = $request->input('harga_tiket');
                $newEvent->jumlah_tiket = $request->input('jumlah_tiket');
                $newEvent->terjual = $request->input('terjual');

                $newEvent->save();

                DB::commit();
                return response()->json(["message"=>"Success"], 200);
            }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=> $e->getMessage ], 500);
        }
        
    }
    
}
